/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mdisc;

import java.io.PrintStream;
import java.math.BigInteger;
import java.util.Random;
import java.util.Scanner;

/**
 *
 * @author Filipe Azevedo
 */
public class MDISC {
    
    public static void main(String[] args) {
        MDISC programa = new MDISC();
        programa.menu();
                
    }

    private final Scanner entrada = new Scanner(System.in);
    private final PrintStream saida = System.out; 
    private static int[][] matriz;  
    private static int[] p; 
    private static int somaMax; 
    private static int[] seq;
    int c; 
    int opcao;
    int ordem; 
    long tempoInicial;
    long tempoExec;
    String espacamento = " ";

    public void menu(){
        
        Scanner ler2 = new Scanner(System.in);
        System.out.println(espacamento);
        System.out.println("Insira a operação que deseja efetuar:");
        System.out.println(espacamento);
        System.out.println("1 - Matriz inserida, sequência e soma máxima ");
        System.out.println("2 - Matriz aleatoria, sequência e soma máxima ");
        System.out.println("3 - Fatorial");
        System.out.println("4 - Sequencia em N elementos");        
        System.out.println("5 - TERMINAR   ");
        System.out.println(espacamento);        
        System.out.print("Insira a opção: ");
        opcao = ler2.nextInt();
        System.out.println(espacamento);
        switch(opcao){
            case (1):
                executarMatrizInserida();
                break;
            case (2):
                executarMatrizAleatoria();
                break;
            case (3):
                executarFatorial();
                break;
            case (4):
                executarSequenciaInserida();
                break;
            case (5):
                System.exit(0);
                break;              
        }        
    }
    
    /**
     * Metodo responsavel por imprimir todas as sequencias para o n inserido
     */
    
    public void executarSequenciaInserida(){
        saida.println("Insira um valor para n: ");
        int n = entrada.nextInt();
        
        int v[] = new int[n];
        for (int y=0; y<n; y++){
            v[y]=y;
        }
        permuta1(v);
        
        saida.println(espacamento);
        saida.println("Para o N inserido existem " + fatorial(n) + " combinações.");
        saida.println(espacamento);
        somaMax=0;
        menu();
    }
        
    public void executarMatrizInserida(){
        
        // Introdução dos valores da matriz no formato x,y,z,...
        
        saida.println("Introduza os valores no formato A,B,C,D: ");
        saida.println(" A  B ");
        saida.println(" C  D ");
        saida.println("Valores: ");
        String stringDeNumeros = entrada.nextLine();
        
        // converte a string para um vetor de strings 
        
        String[] vetorString = stringDeNumeros.split(",");
        
        int[] vetorInt = stringsParaInts(vetorString);
        saida.println("Matriz introduzida: ");
        
        // converte o vetor para uma matriz
        
        matriz = converteVetorEmMatriz(vetorInt);
        
        // Cria a sequencia de n elementos para a ordem da matriz
        
        ordem = matriz.length;
        int v[] = new int[ordem];
        for (int y=0; y<ordem; y++){
            v[y]=y;
        }
        
        // imprime a matriz
        
        imprimirMatriz(matriz);
        
        tempoInicial = System.currentTimeMillis();
        
        // Inicia o metodo responsavel por gerar as combinações
        
        tempoInicial = System.currentTimeMillis();
        permuta1(v);
        tempoExec = System.currentTimeMillis() - tempoInicial;
        
        imprimirResultados();
        
        // Retorna para o menu
        somaMax=0;
        menu();
            
    }
    
    /*
    * Metodo responsavel por gerar todas as sequencia de n valores.
    * Para cada n elementos exsitem n! combinaçoes possiveis.
    * Metodo recursivo que implementa as permutações
    */
    
    private void permuta(int []vet, int n) {
         
        if (n==vet.length) {
            if (opcao==4){
                imprimir();
            }
            else{
                sequencias();    
            }
            
                             
        } else {
                     
            for (int i=0; i < vet.length; i++) {
             
                boolean enc = false;
             
                for (int j = 0; j < n; j++) {
                 
                    if (p[j]==vet[i]) enc = true;
                }
             
                if (!enc) {
                     
                    p[n] = vet[i];
                    permuta(vet,n+1);
                }
                 
            } 
             
        } 
         
    }
    
    /*
     * Metodo principal: recebe o vetor com os elemntos que serão permutados
     */
    
    public void permuta1(int [] vet) {
         
        p = new int[vet.length];
        seq = new int[p.length];
        permuta(vet,0);
        
    }
    
   /*
    * Para cada sequencia gerada, faz a soma dos seus termos e verifica se é a soma maxima
    */ 
    
    public void sequencias(){
        
        int soma=0;
        for (int i=0; i < p.length; i++) {           
            
            if ( i+1==p.length){
                
                soma=soma+matriz[p[0]][p[p.length-1]];   
            }
            else {
                
                soma=soma+matriz[p[i]][p[i+1]];
            }  
            somaMaximaSeq(soma);    
        }
            
    }
    
    /*
    * Verifica se a soma é superior à soma dos elementos anteriores. 
    * Se for, guarda a somamaxima e a sequencia correspondente.
    */
    
    public void somaMaximaSeq(int soma){
                        
        if (soma>somaMax){
            somaMax=soma;
            for (int i=0; i < p.length; i++) {
                seq[i]=p[i];
            }
        }
    }
    
    /*
    * Imprime todas as sequencias possiveis  em n elemntos
    */
    
    public void imprimir(){
        
        for (int i=0; i < p.length; i++) {
            
            System.out.print(p[i] + " ");
            
        } System.out.println(espacamento);
    
    }
    
    /*
    * Executa o programa para uma ordem inserida
    */
     
    public void executarMatrizAleatoria(){
        
        saida.println(espacamento);
        saida.println("Insira a ordem da matriz a gerar: ");
        ordem = Integer.parseInt(entrada.nextLine());
        
        // gerar matrizes aleatoriamente com valores entre [0,100] com as diagonais em zero.
        
        int[][] matrizAleatoria = gerarMatriz(ordem, 100);
        saida.println(espacamento);

        // Imprimir matriz gerada aleatoriamente
        
        saida.println("Matriz gerada aleatoriamente de ordem " + ordem + ":");
        saida.println(espacamento);
        
        // Imprime matriz gerada aleatoriamenre
        
        imprimirMatriz(matrizAleatoria);
        // Cria a sequencia de n elementos para a ordem inserida da matriz 
        int v[] = new int[ordem];
        
        for (int y=0; y<ordem; y++){
            v[y]=y;
        }
        tempoInicial = System.currentTimeMillis();
        permuta1(v);
        tempoExec = System.currentTimeMillis() - tempoInicial;
                
        imprimirResultados();
        somaMax=0;
        menu();
        
        
    }
    
    public void imprimirResultados(){
        
        System.out.println(espacamento);
        System.out.println("Soma máxima: " + somaMax);
        System.out.println("\nPara a matriz de ordem " + ordem + " existem " + fatorial(ordem) + " sequências possiveis.");
        System.out.println(espacamento);
        System.out.println("Sequência com soma máxima: " );
        for (int y=0; y<seq.length;y++) System.out.print(seq[y] + " ");
        System.out.println(espacamento);
        System.out.println("\nO tempo de execução foi de " + tempoExec + " milissegundos");
        System.out.println(espacamento);
        System.out.println("----------------------------------------------------------------------");
    }
    
        
    /*
    * Converte o vetor anteriormente convertido em int para uma matriz quadrada
    */
    
    private int[][] converteVetorEmMatriz(int[] vetor) {
        
        ordem = (int) Math.sqrt(vetor.length);
        matriz = new int[ordem][ordem];
        try {
            int posicao = 0;
            for (int linha = 0; linha < ordem; linha++) {
                for (int coluna = 0; coluna < ordem; coluna++) {
                    matriz[linha][coluna] = vetor[posicao++];
                }
            }
        } catch (ArrayIndexOutOfBoundsException e) {
            saida.println("Erro: Não introduziu valores correspondentes a uma matriz quadrada! " + e);
        }
        return matriz;
    
    }
    
    public void executarFatorial(){
        
        System.out.println("Insira um valor: ");
        int valor = entrada.nextInt();
        try {            
            System.out.println(fatorial(valor));
        } catch (ArrayIndexOutOfBoundsException e) {
            saida.println("Erro: Não introduziu valores correspondentes a uma matriz quadrada! " + e);
        }
        menu();
    }
    
    /*
    * Calcula o fatorial de um valor. Utilizamos o bigInteger para permitir calcular valores elevados
    */
    
    private int fatorial(int valor) {
        
        int factorial = 1;
        for (int i = 1; i <= valor; i++) {
            factorial = factorial*i;
        }
        return factorial;
    }

    /**
     * Gerar matrizes de ordem informada aleatoriamente com valores entre 0 e o 100, com as diagonais em zero.
     */
    
    private int[][] gerarMatriz(int ordem, int limite) {
        
        Random gerador = new Random();
        matriz = new int[ordem][ordem];
        for (int linha = 0; linha < ordem; linha++) {
            for (int coluna = 0; coluna < ordem; coluna++) {
                if (linha == coluna){
                    matriz[linha][coluna] = 0;
                }
                else {
                    int ger = gerador.nextInt(limite);
                    matriz[linha][coluna]=ger;
                    matriz[coluna][linha]=ger;
                }
            }
        }
        return matriz;
    }

    /**
     * Imprime a matriz passada pelo parametro
     */
    
    private void imprimirMatriz(int[][] matriz) {
        String espaco = " ";
        for (int linha = 0; linha < matriz.length; linha++) {
            for (int coluna = 0; coluna < matriz[linha].length; coluna++) {
                if (matriz[linha][coluna]<10){
                    espaco = " ";
                }
                else {
                    espaco = "";
                }
                saida.print(espaco + matriz[linha][coluna] + "  ");
            }
            saida.println(espacamento);
        }
    }

    /**
     * Converte um vetor de String num vetor de inteiros
     */
    
    private int[] stringsParaInts(String[] strings) {
        
        int ints[] = new int[strings.length];
        try {
            for (int i = 0; i < strings.length; ++i) {
                ints[i] = Integer.parseInt(strings[i].trim());
            }
        } catch (NumberFormatException e) {
            saida.println("Erro: Não introduziu apenas numeros! " + e);
        }
        return ints;
    }
}